## Summary

This collection of Terraform code is NOT for production but is designed to demonstrate AWS networking concepts which are growing in popularity.  Customers seeking to have a hybrid cloud setup between their on premise networks and in-cloud networks may use on premise IP addressing allocated to cloud-based VPC resources.  Cloud adoption can quickly outpace the availability of IP addresses from the on-premise network space and to mitigate these several patterns can be adopted.

The first is to use a combination of small routable CIDRs in a VPC along with a much larger, non-routable IP CIDR to host reousrces.  These resources may need to be able to communicate with on premise and may even need to be addressed by on premise.  To allow this an NLB can be created within the small routable CIDR to relay external traffic to internal resources.  Equally a NAT instance can be used to direct and mask traffic from the internal non-routable CIDR to the external networks.

To demonstrate this you can communicate with the application from a client over the Transit Gateway in Ireland.

Another pattern is the ability to stretch an application and make it accessible from other AWS regions.  A pattern demonstrated in this code base is to create a 2nd peered VPC in the target region, create an NLB in the 2nd peered VPC, have the NLB direct traffic to the IP addresses of the source VPC application, potentially in the non-routable CIDR range, and then expose the extended NLB as an Endpoint Service to other accounts.

To demonstrate this you can visit the London region and obtain the hostname of the VPC endpoint in the Client VPC.  Then, using Session Manager, you can connect to the client EC2 instance in the Client VPC and send an HTTP GET request to the application in Ireland through the endpoint:

```bash
curl http://<vpc-endpoint-url>
```

The document returned will be an HTML document from the web server located in the non-routable CIDR of the application VPC in Ireland.
