# create 4 VPCs, 2 in Ireland, 2 in London
# in Ireland: VPC-SharedServices, VPC-Application
# in London: VPC-Application-Extend, VPC-Client

variable "ss_vpc_id" {
  type = string
}

variable "ss_vpc_subnet_ids" {
  type = list(string)
}

variable "ss_vpc_route_table_id_0" {
  type = string
}
variable "ss_vpc_route_table_id_1" {
  type = string
}
variable "ss_vpc_route_table_id_2" {
  type = string
}

variable "application_vpc_id" {
  type = string
}

variable "application_vpc_subnet_ids" {
  type = list(string)
}

variable "application_vpc_route_table_id_0" {
  type = string
}
variable "application_vpc_route_table_id_1" {
  type = string
}
variable "application_vpc_route_table_id_2" {
  type = string
}

## TRANSIT GATEWAY SETUP
## 
resource "aws_ec2_transit_gateway" "ss_tgw" {
  description                     = "ireland-ss-tgw"
  auto_accept_shared_attachments  = "enable"
  default_route_table_association = "disable"
  default_route_table_propagation = "disable"
  dns_support                     = "enable"

  tags = {
    Terraform   = "true"
    Name        = "ss-tgw"
    Environment = "ss"
  }
}

resource "aws_ec2_transit_gateway_route_table" "ss_tgw_rt" {
  transit_gateway_id = aws_ec2_transit_gateway.ss_tgw.id

  tags = {
    Terraform   = "true"
    Name        = "ss-tgw-rt"
    Environment = "ss"
  }
}

## ATTACH SHARED SERVICE VPC
##
resource "aws_ec2_transit_gateway_vpc_attachment" "ss_vpc_attachment" {
  subnet_ids                                      = var.ss_vpc_subnet_ids
  transit_gateway_id                              = aws_ec2_transit_gateway.ss_tgw.id
  vpc_id                                          = var.ss_vpc_id
  dns_support                                     = "enable"
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false
}

resource "aws_ec2_transit_gateway_route_table_propagation" "ss_tgw_rt_prop" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.ss_vpc_attachment.id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.ss_tgw_rt.id
}

resource "aws_ec2_transit_gateway_route_table_association" "ss_tgw_rt_assoc" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.ss_vpc_attachment.id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.ss_tgw_rt.id
}

data "aws_vpc" "ss_vpc" {
  id = var.ss_vpc_id
}

resource "aws_route" "ss_route_table_update_0" {
  route_table_id         = var.ss_vpc_route_table_id_0
  destination_cidr_block = data.aws_vpc.application_vpc.cidr_block
  transit_gateway_id     = aws_ec2_transit_gateway.ss_tgw.id
}
resource "aws_route" "ss_route_table_update_1" {
  route_table_id         = var.ss_vpc_route_table_id_1
  destination_cidr_block = data.aws_vpc.application_vpc.cidr_block
  transit_gateway_id     = aws_ec2_transit_gateway.ss_tgw.id
}
resource "aws_route" "ss_route_table_update_2" {
  route_table_id         = var.ss_vpc_route_table_id_2
  destination_cidr_block = data.aws_vpc.application_vpc.cidr_block
  transit_gateway_id     = aws_ec2_transit_gateway.ss_tgw.id
}

## ATTACH APPLICATION VPC
##
data "aws_vpc" "application_vpc" {
  id = var.application_vpc_id
}

resource "aws_ec2_transit_gateway_vpc_attachment" "application_vpc_attachment" {
  subnet_ids                                      = var.application_vpc_subnet_ids
  transit_gateway_id                              = aws_ec2_transit_gateway.ss_tgw.id
  vpc_id                                          = var.application_vpc_id
  dns_support                                     = "enable"
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false
}

resource "aws_ec2_transit_gateway_route_table_propagation" "application_tgw_rt_prop" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.application_vpc_attachment.id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.ss_tgw_rt.id
}

resource "aws_ec2_transit_gateway_route_table_association" "application_tgw_rt_assoc" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.application_vpc_attachment.id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.ss_tgw_rt.id
}

resource "aws_route" "application_route_table_update_0" {
  route_table_id         = var.application_vpc_route_table_id_0
  destination_cidr_block = data.aws_vpc.ss_vpc.cidr_block
  transit_gateway_id     = aws_ec2_transit_gateway.ss_tgw.id
}
resource "aws_route" "application_route_table_update_1" {
  route_table_id         = var.application_vpc_route_table_id_1
  destination_cidr_block = data.aws_vpc.ss_vpc.cidr_block
  transit_gateway_id     = aws_ec2_transit_gateway.ss_tgw.id
}
resource "aws_route" "application_route_table_update_2" {
  route_table_id         = var.application_vpc_route_table_id_2
  destination_cidr_block = data.aws_vpc.ss_vpc.cidr_block
  transit_gateway_id     = aws_ec2_transit_gateway.ss_tgw.id
}