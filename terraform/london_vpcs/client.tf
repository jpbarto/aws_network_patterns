# create 4 VPCs, 2 in Ireland, 2 in London
# in Ireland: VPC-SharedServices, VPC-Application
# in London: VPC-Application-Extend, VPC-Client

locals {
  default_sg_id = module.application_client_vpc.default_security_group_id
  client_vpc_private_subnets = module.application_client_vpc.private_subnets
}

# Application Client VPC (London)
module "application_client_vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "strat-app-client-vpc"
  cidr = "10.4.0.0/16"

  azs             = ["eu-west-2a", "eu-west-2b", "eu-west-2c"]
  private_subnets = ["10.4.1.0/24", "10.4.2.0/24", "10.4.3.0/24"]

  enable_nat_gateway = false
  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_ssm_endpoint              = true
  ssm_endpoint_private_dns_enabled = true
  ssm_endpoint_security_group_ids  = [local.default_sg_id]
  ssm_endpoint_subnet_ids          = local.client_vpc_private_subnets

  enable_ssmmessages_endpoint              = true
  ssmmessages_endpoint_private_dns_enabled = true
  ssmmessages_endpoint_security_group_ids  = [local.default_sg_id]
  ssmmessages_endpoint_subnet_ids          = local.client_vpc_private_subnets

  enable_ec2messages_endpoint              = true
  ec2messages_endpoint_private_dns_enabled = true
  ec2messages_endpoint_security_group_ids  = [local.default_sg_id]
  ec2messages_endpoint_subnet_ids          = local.client_vpc_private_subnets

  tags = {
    Terraform   = "true"
    Environment = "application-client"
  }
}

resource "aws_vpc_endpoint" "application_service" {
  vpc_id            = module.application_client_vpc.vpc_id
  service_name      = aws_vpc_endpoint_service.application_ext_endpoint_service.service_name
  vpc_endpoint_type = "Interface"

  security_group_ids = [local.default_sg_id]

  subnet_ids          = local.client_vpc_private_subnets
  private_dns_enabled = false
}

data "aws_ami" "ubuntu_london" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "ss_proxy" {
  ami                  = data.aws_ami.ubuntu_london.id
  instance_type        = "t3.micro"
  subnet_id            = module.application_client_vpc.private_subnets[0]
  iam_instance_profile = var.ec2_ssm_instance_profile

  tags = {
    Terraform   = "true"
    Name        = "application-client"
    Environment = "client"
  }
}