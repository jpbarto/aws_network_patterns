# create 4 VPCs, 2 in Ireland, 2 in London
# in Ireland: VPC-SharedServices, VPC-Application
# in London: VPC-Application-Extend, VPC-Client

# Application Extended VPC (London)
module "application_ext_vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "strat-app-ext-vpc"
  cidr = "10.3.0.0/16"

  azs             = ["eu-west-2a", "eu-west-2b", "eu-west-2c"]
  private_subnets = ["10.3.1.0/24", "10.3.2.0/24", "10.3.3.0/24"]

  enable_nat_gateway = false

  tags = {
    Terraform   = "true"
    Environment = "application-extended"
  }
}

resource "aws_vpc_peering_connection_accepter" "peer" {
  vpc_peering_connection_id = var.application_peer_conn_id
  auto_accept               = true
}

resource "aws_route" "application_route_0" {
  route_table_id            = module.application_ext_vpc.private_route_table_ids[0]
  destination_cidr_block    = var.application_vpc_cidr
  vpc_peering_connection_id = var.application_peer_conn_id
}
resource "aws_route" "application_route_1" {
  route_table_id            = module.application_ext_vpc.private_route_table_ids[1]
  destination_cidr_block    = var.application_vpc_cidr
  vpc_peering_connection_id = var.application_peer_conn_id
}
resource "aws_route" "application_route_2" {
  route_table_id            = module.application_ext_vpc.private_route_table_ids[2]
  destination_cidr_block    = var.application_vpc_cidr
  vpc_peering_connection_id = var.application_peer_conn_id
}

module "application_ext_nlb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 5.0"

  name = "application-ext-nlb"

  load_balancer_type = "network"

  vpc_id   = module.application_ext_vpc.vpc_id
  subnets  = module.application_ext_vpc.private_subnets
  internal = true

  target_groups = [
    {
      name_prefix      = "appex-"
      backend_protocol = "TCP"
      backend_port     = 80
      target_type      = "ip"
    }
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "TCP"
      target_group_index = 0
    }
  ]

  tags = {
    Terraform   = "true"
    Environment = "application"
  }
}

resource "aws_lb_target_group_attachment" "application_nlb_ip_0" {
  target_group_arn  = module.application_ext_nlb.target_group_arns[0]
  target_id         = var.application_ext_nlb_target_ip_0
  availability_zone = "all"
}

resource "aws_lb_target_group_attachment" "application_nlb_ip_1" {
  target_group_arn  = module.application_ext_nlb.target_group_arns[0]
  target_id         = var.application_ext_nlb_target_ip_1
  availability_zone = "all"
}

resource "aws_lb_target_group_attachment" "application_nlb_ip_2" {
  target_group_arn  = module.application_ext_nlb.target_group_arns[0]
  target_id         = var.application_ext_nlb_target_ip_2
  availability_zone = "all"
}

resource "aws_vpc_endpoint_service" "application_ext_endpoint_service" {
  acceptance_required        = false
  network_load_balancer_arns = [module.application_ext_nlb.this_lb_arn]
}