output "application_ext_vpc_id" {
  value = module.application_ext_vpc.vpc_id
}

output "application_ext_vpc_cidr" {
  value = module.application_ext_vpc.vpc_cidr_block
}

output "application_ext_nlb_target_arns" {
  value = module.application_ext_nlb.target_group_arns
}

output "client_vpc_cidr" {
  value = module.application_client_vpc.vpc_cidr_block
}