variable "application_peer_conn_id" {
  type = string
}

variable "application_vpc_cidr" {
  type = string
}

variable "application_ext_nlb_target_ip_0" {
  type = string
}

variable "application_ext_nlb_target_ip_1" {
  type = string
}

variable "application_ext_nlb_target_ip_2" {
  type = string
}

variable "ec2_ssm_instance_profile" {
  type = string
}