# create 4 VPCs, 2 in Ireland, 2 in London
# in Ireland: VPC-SharedServices, VPC-Application
# in London: VPC-Application-Extend, VPC-Client

# Shared Service VPC (Ireland)
module "vpc_ss" {
  source = "terraform-aws-modules/vpc/aws"

  name = "strat-sharedservice-vpc"
  cidr = "10.1.0.0/16"

  azs             = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  private_subnets = ["10.1.1.0/24", "10.1.2.0/24", "10.1.3.0/24"]
  public_subnets  = ["10.1.101.0/24", "10.1.102.0/24", "10.1.103.0/24"]

  enable_nat_gateway = true

  tags = {
    Terraform   = "true"
    Environment = "shared-services"
  }
}

resource "aws_security_group_rule" "ss_proxy_ingress" {
  type              = "ingress"
  from_port         = 3128
  to_port           = 3128
  protocol          = "tcp"
  cidr_blocks       = [module.application_vpc.vpc_cidr_block]
  security_group_id = module.vpc_ss.default_security_group_id
}

resource "aws_instance" "ss_proxy" {

  ami                  = data.aws_ami.ubuntu_ireland.id
  instance_type        = "t3.micro"
  subnet_id            = module.vpc_ss.private_subnets[1]
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.id

  user_data = <<EOF
#!/bin/bash
apt-get update -y
apt-get install -y squid
echo 'http_access allow localnet' >> /etc/squid/conf.d/debian.conf
systemctl restart squid
EOF

  tags = {
    Terraform   = "true"
    Name        = "ss-proxy"
    Environment = "ss"
  }
}
