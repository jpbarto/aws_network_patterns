output "application_vpc_subnet_ids" {
  value = local.application_vpc_private_subnets
}

output "application_vpc_id" {
  value = module.application_vpc.vpc_id
}

output "application_vpc_cidr" {
  value = module.application_vpc.vpc_cidr_block
}

output "application_vpc_route_table_id_0" {
  value = module.application_vpc.private_route_table_ids[0]
}
output "application_vpc_route_table_id_1" {
  value = module.application_vpc.private_route_table_ids[1]
}
output "application_vpc_route_table_id_2" {
  value = module.application_vpc.private_route_table_ids[2]
}

output "application_peer_conn_id" {
  value = aws_vpc_peering_connection.application_ext_peer.id
}

output "application_nlb_dns_name" {
  value = module.application_nlb.this_lb_dns_name
}

output "ss_vpc_id" {
  value = module.vpc_ss.vpc_id
}

output "ss_vpc_subnet_ids" {
  value = module.vpc_ss.private_subnets
}

output "ss_vpc_route_table_id_0" {
  value = module.vpc_ss.private_route_table_ids[0]
}
output "ss_vpc_route_table_id_1" {
  value = module.vpc_ss.private_route_table_ids[1]
}
output "ss_vpc_route_table_id_2" {
  value = module.vpc_ss.private_route_table_ids[2]
}

output "ec2_ssm_instance_profile" {
  value =  aws_iam_instance_profile.ec2_profile.id
}