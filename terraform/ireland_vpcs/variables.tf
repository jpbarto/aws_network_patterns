variable "application_ext_vpc_id" {
  type = string
}

variable "application_ext_vpc_cidr" {
  type = string
}

variable "client_vpc_cidr" {
  type = string
}

variable "application_ext_nlb_target_arns" {
  type = list(string)
}