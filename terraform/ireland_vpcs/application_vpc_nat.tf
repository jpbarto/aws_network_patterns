## NAT Gateway
##

resource "aws_instance" "nat_gw" {
  ami                  = data.aws_ami.ubuntu_ireland.id
  instance_type        = "t3.micro"
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.id

  network_interface {
    network_interface_id = aws_network_interface.primary_nic.id
    device_index         = 0
  }
  network_interface {
    network_interface_id = aws_network_interface.secondary_nic.id
    device_index         = 1
  }

  user_data = <<EOF
#!/bin/bash
apt-get update -y
apt-get install -y iptables
sysctl -w net.ipv4.ip_forward=1
echo 'net.ipv4.ip_forward=1' >> /etc/sysctl.conf
iptables -t nat -A POSTROUTING -o ens5 -j MASQUERADE
EOF
  tags = {
    Terraform   = "true"
    Name        = "nat-gateway"
    Environment = "application"
  }
}

resource "aws_network_interface" "primary_nic" {
  subnet_id         = module.application_vpc.private_subnets[0]
  source_dest_check = false

  tags = {
    Terraform   = "true"
    Name        = "nat-gateway-eth0"
    Environment = "application"
  }
}

resource "aws_network_interface" "secondary_nic" {
  subnet_id         = module.application_vpc.private_subnets[0]
  source_dest_check = false

  tags = {
    Terraform   = "true"
    Name        = "nat-gateway-eth1"
    Environment = "application"
  }
}