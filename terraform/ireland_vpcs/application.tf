# create 4 VPCs, 2 in Ireland, 2 in London
# in Ireland: VPC-SharedServices, VPC-Application
# in London: VPC-Application-Extend, VPC-Client

# Application VPC (Ireland)
data "aws_security_group" "default" {
  name   = "default"
  vpc_id = module.application_vpc.vpc_id
}


## Application Server
##

module "application_nlb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 5.0"

  name = "application-nlb"

  load_balancer_type = "network"

  vpc_id   = module.application_vpc.vpc_id
  subnets  = module.application_vpc.private_subnets
  internal = true

  target_groups = [
    {
      name_prefix      = "app-"
      backend_protocol = "TCP"
      backend_port     = 80
      target_type      = "instance"
    }
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "TCP"
      target_group_index = 0
    }
  ]

  tags = {
    Terraform   = "true"
    Environment = "application"
  }
}

resource "aws_security_group_rule" "allow_nlb_health_check" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = [module.application_vpc.vpc_cidr_block]
  security_group_id = data.aws_security_group.default.id
}

resource "aws_security_group_rule" "allow_ext_nlb_health_check" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = [var.application_ext_vpc_cidr]
  security_group_id = data.aws_security_group.default.id
}

resource "aws_security_group_rule" "allow_client_access" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = [var.client_vpc_cidr]
  security_group_id = data.aws_security_group.default.id
}

resource "aws_security_group_rule" "allow_ss_access" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = [module.vpc_ss.vpc_cidr_block]
  security_group_id = data.aws_security_group.default.id
}

data "aws_ami" "ubuntu_ireland" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_iam_role" "ec2_role" {
  name = "strat-ec2-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    Terraform   = "true"
    Environment = "application"
  }
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

resource "aws_iam_instance_profile" "ec2_profile" {
  name = "ec2-instance-profile"
  role = aws_iam_role.ec2_role.name
}

module "application_asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"

  name = "strat-app-asg"

  # Launch configuration
  lc_name = "strat-app-lc"

  image_id             = data.aws_ami.ubuntu_ireland.id
  instance_type        = "t3.micro"
  security_groups      = [data.aws_security_group.default.id]
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.id
  user_data            = <<EOF
#!/bin/bash

echo 'http_proxy=http://${aws_instance.ss_proxy.private_ip}:3128' >> /etc/environment
echo 'https_proxy=http://${aws_instance.ss_proxy.private_ip}:3128' >> /etc/environment
echo 'Acquire::http::Proxy "http://${aws_instance.ss_proxy.private_ip}:3128/";' >> /etc/apt/apt.conf.d/00proxy
echo 'Acquire::https::Proxy "http://${aws_instance.ss_proxy.private_ip}:3128/";' >> /etc/apt/apt.conf.d/00proxy

apt-get update -y
apt-get install -y nginx

echo "<html><head><title>Hello from $HOSTNAME</title></head><body><p>This is a simple Nginx page from $HOSTNAME</p></body></html>" > /var/www/html/index.nginx-debian.html
EOF

  # Auto scaling group
  asg_name                  = "application-asg"
  vpc_zone_identifier       = module.application_vpc.intra_subnets
  health_check_type         = "EC2"
  min_size                  = 1
  max_size                  = 5
  desired_capacity          = 3
  wait_for_capacity_timeout = 0
  target_group_arns         = module.application_nlb.target_group_arns # concat(module.application_nlb.target_group_arns, var.application_ext_nlb_target_arns)

  tags = [
    {
      key                 = "Terraform"
      value               = "true"
      propagate_at_launch = true
    },
    {
      key                 = "Project"
      value               = "application"
      propagate_at_launch = true
    },
  ]
}


