locals {
  application_vpc_private_subnets = module.application_vpc.private_subnets
}

module "application_vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name                  = "strat-app-vpc"
  cidr                  = "10.2.0.0/16"
  secondary_cidr_blocks = ["100.64.0.0/16"]

  azs             = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  private_subnets = ["10.2.1.0/24", "10.2.2.0/24", "10.2.3.0/24"]
  intra_subnets   = ["100.64.1.0/24", "100.64.2.0/24", "100.64.3.0/24"]

  enable_nat_gateway   = false
  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_s3_endpoint = true

  enable_ssm_endpoint              = true
  ssm_endpoint_private_dns_enabled = true
  ssm_endpoint_security_group_ids  = [data.aws_security_group.default.id]
  ssm_endpoint_subnet_ids          = local.application_vpc_private_subnets

  enable_ssmmessages_endpoint              = true
  ssmmessages_endpoint_private_dns_enabled = true
  ssmmessages_endpoint_security_group_ids  = [data.aws_security_group.default.id]
  ssmmessages_endpoint_subnet_ids          = local.application_vpc_private_subnets

  enable_ec2messages_endpoint              = true
  ec2messages_endpoint_private_dns_enabled = true
  ec2messages_endpoint_security_group_ids  = [data.aws_security_group.default.id]
  ec2messages_endpoint_subnet_ids          = local.application_vpc_private_subnets

  tags = {
    Terraform   = "true"
    Environment = "application"
  }
}

resource "aws_vpc_peering_connection" "application_ext_peer" {
  peer_vpc_id = var.application_ext_vpc_id
  peer_region = "eu-west-2"
  vpc_id      = module.application_vpc.vpc_id

  auto_accept = false
}

resource "aws_route" "application_intra_nat_route_0" {
  route_table_id         = module.application_vpc.intra_route_table_ids[0]
  destination_cidr_block = "0.0.0.0/0"
  network_interface_id   = aws_network_interface.secondary_nic.id
}

resource "aws_route" "application_ext_route_0" {
  route_table_id            = module.application_vpc.private_route_table_ids[0]
  destination_cidr_block    = var.application_ext_vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.application_ext_peer.id
}
resource "aws_route" "application_ext_route_1" {
  route_table_id            = module.application_vpc.private_route_table_ids[1]
  destination_cidr_block    = var.application_ext_vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.application_ext_peer.id
}
resource "aws_route" "application_ext_route_2" {
  route_table_id            = module.application_vpc.private_route_table_ids[2]
  destination_cidr_block    = var.application_ext_vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.application_ext_peer.id
}