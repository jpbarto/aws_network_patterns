module "ireland_vpcs" {
  source = "./ireland_vpcs"
  providers = {
    aws = aws.ireland
  }

  application_ext_vpc_id          = module.london_vpcs.application_ext_vpc_id
  application_ext_vpc_cidr        = module.london_vpcs.application_ext_vpc_cidr
  application_ext_nlb_target_arns = module.london_vpcs.application_ext_nlb_target_arns
  client_vpc_cidr                 = module.london_vpcs.client_vpc_cidr
}

data "dns_a_record_set" "application_nlb_ips" {
  host = module.ireland_vpcs.application_nlb_dns_name
}

module "london_vpcs" {
  source = "./london_vpcs"
  providers = {
    aws = aws.london
  }

  application_peer_conn_id        = module.ireland_vpcs.application_peer_conn_id
  application_vpc_cidr            = module.ireland_vpcs.application_vpc_cidr
  application_ext_nlb_target_ip_0 = data.dns_a_record_set.application_nlb_ips.addrs[0]
  application_ext_nlb_target_ip_1 = data.dns_a_record_set.application_nlb_ips.addrs[1]
  application_ext_nlb_target_ip_2 = data.dns_a_record_set.application_nlb_ips.addrs[2]
  ec2_ssm_instance_profile = module.ireland_vpcs.ec2_ssm_instance_profile
}

module "transit_gw" {
  source = "./tgw"
  providers = {
    aws = aws.ireland
  }

  ss_vpc_id               = module.ireland_vpcs.ss_vpc_id
  ss_vpc_subnet_ids       = module.ireland_vpcs.ss_vpc_subnet_ids
  ss_vpc_route_table_id_0 = module.ireland_vpcs.ss_vpc_route_table_id_0
  ss_vpc_route_table_id_1 = module.ireland_vpcs.ss_vpc_route_table_id_1
  ss_vpc_route_table_id_2 = module.ireland_vpcs.ss_vpc_route_table_id_2

  application_vpc_id               = module.ireland_vpcs.application_vpc_id
  application_vpc_subnet_ids       = module.ireland_vpcs.application_vpc_subnet_ids
  application_vpc_route_table_id_0 = module.ireland_vpcs.application_vpc_route_table_id_0
  application_vpc_route_table_id_1 = module.ireland_vpcs.application_vpc_route_table_id_1
  application_vpc_route_table_id_2 = module.ireland_vpcs.application_vpc_route_table_id_2
}